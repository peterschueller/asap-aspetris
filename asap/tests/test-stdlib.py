#!/usr/bin/python 

import sys, os
# load first from here
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),'../../'))

from asap import core
from asap.core import api
from asap.plugins import stdlib

import time

def testChop(chop):
  print "TESTING chop"
  inp = api.ExternalInput(environment=None, inputs=('foobar',))
  print "using input {}".format(inp)
  res = chop.query(inp)
  print "result: {}".format(res)
  inp.inputs = (res[0][0],)
  print "using input {}".format(inp)
  res = chop.query(inp)
  print "result: {}".format(res)

def testConcatenate(concat):
  print "TESTING concat nonquoted"
  inp = api.ExternalInput(environment=None, inputs=('foobar','baz','boo',))
  print "using input {}".format(inp)
  res = concat.query(inp)
  print "result: {}".format(res)

  print "TESTING concat quoted"
  inp = api.ExternalInput(environment=None, inputs=('foobar','"baz"','boo',))
  print "using input {}".format(inp)
  res = concat.query(inp)
  print "result: {}".format(res)

  print "TESTING concat int"
  inp = api.ExternalInput(environment=None, inputs=('foobar','"baz"',0,))
  print "using input {}".format(inp)
  res = concat.query(inp)
  print "result: {}".format(res)

def testRandom(random):
  print "TESTING random"
  for i in range(0,5):
    inp = api.ExternalInput(environment=None, inputs=(0,5))
    print "using input {}".format(inp)
    res = random.query(inp)
    print "result: {}".format(res)

    inp = api.ExternalInput(environment=None, inputs=(3,7))
    print "using input {}".format(inp)
    res = random.query(inp)
    print "result: {}".format(res)

def main():
  pl = stdlib.Plugin()
  externals = pl.externals()
  chop, concat, random = externals
  testChop(chop)
  testConcatenate(concat)
  testRandom(random)

if __name__ == '__main__':
  main()

