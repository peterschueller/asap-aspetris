#!/usr/bin/python 

import sys, os
# load first from here
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),'../../'))

from asap.core import api
from asap.plugins import tetris

import time

def main():
  pl = tetris.Plugin()
  externals = pl.externals()
  delayFromLevel = externals[0]
  for level in range(0,10):
    print "calling delayFromLevel on level {}".format(level)
    inputs = api.ExternalInput(environment=None, inputs=(level,))
    print "using inputs {}".format(inputs)
    res = delayFromLevel.query(inputs)
    print "result: {}".format(res)

if __name__ == '__main__':
  main()

