#!/usr/bin/python 

import sys, os
# load first from here
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),'../../'))

from asap import core
from asap.core import api
from asap.plugins import timer

import time

def main():
  eq = core.EventQueueImpl()
  pl = timer.Plugin()
  pl.setQueue(eq)
  actions = pl.actions()
  startTimer = actions[0]
  print "event queue:", repr(list(eq))
  print "scheduling foo/100"
  startTimer.execute(api.ActionInput(None,['foo',100]))
  print "event queue:", repr(list(eq))
  print "scheduling bar/500"
  startTimer.execute(api.ActionInput(None,['bar',500]))
  print "event queue:", repr(list(eq))
  print "sleeping 250"
  time.sleep(0.25)
  print "event queue:", repr(list(eq))
  print "sleeping 500"
  time.sleep(0.5)
  print "event queue:", repr(list(eq))

if __name__ == '__main__':
  main()

