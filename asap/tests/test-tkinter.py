#!/usr/bin/env python3

import sys, os
# load first from here
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])),'../../'))

from asap import core
from asap.core import api
from asap.plugins import tkinter

import time

def main():
  eq = core.EventQueueImpl()
  pl = tkinter.Plugin()
  pl.setQueue(eq)
  actions = pl.actions()
  drawRectangle = actions[0]
  for it in range(0,5):
    pl.beforeExecution()
    drawRectangle.execute(api.ActionInput(environment=None,
      inputs=[
        api.Term.fromNestedTuple(('rel', (0,))),
        api.Term.fromNestedTuple(('rel', (0,))),
        api.Term.fromNestedTuple(('rel', (100,))),
        api.Term.fromNestedTuple(('rel', (100,))),
        api.Term.fromNestedTuple(('yellow',))]))
    drawRectangle.execute(api.ActionInput(environment=None,
      inputs=[
        api.Term.fromNestedTuple(('rel', (25,))),
        api.Term.fromNestedTuple(('rel', (25,))),
        api.Term.fromNestedTuple(('rel', (75,))),
        api.Term.fromNestedTuple(('rel', (75,))),
        api.Term.fromNestedTuple(('green',))]))
    pl.afterExecution()
    time.sleep(1)
    print("event queue:", repr(list(eq)))
  print("quitting")
  pl.quit()
  print("done")

if __name__ == '__main__':
  main()

