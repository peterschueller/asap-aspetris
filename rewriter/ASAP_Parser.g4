grammar ASAP_Parser;

program : statements? query?;

statements : statement statements?;

query : classical_literal QUERY_MARK;

statement : CONS body DOT
          | head (CONS body?)? DOT
          | WCONS body? DOT SQUARE_OPEN weight_at_level SQUARE_CLOSE
          | gringo_sharp;   // syntax extension

head : asap_action | disjunction | choice;

body : ( naf_literal | NAF? aggregate ) (COMMA body)?;

disjunction : classical_literal (OR disjunction)?;

choice : (smt=term smop=binop)? CURLY_OPEN choice_elements? CURLY_CLOSE (lgop=binop lgt=term)?;

choice_elements : choice_element (SEMICOLON choice_elements)?;

choice_element : classical_literal (COLON naf_literals?)?;

aggregate : (smt=term smop=binop)? aggregate_function CURLY_OPEN aggregate_elements CURLY_CLOSE (lgop=binop lgt=term)?;

aggregate_elements : aggregate_element (SEMICOLON aggregate_elements)?;

aggregate_element : basic_terms? (COLON naf_literals?)?;

aggregate_function : AGGREGATE_COUNT | AGGREGATE_MAX | AGGREGATE_MIN | AGGREGATE_SUM;

weight_at_level : term (AT term)? (COMMA terms)?;

naf_literals : naf_literal (COMMA naf_literals)?;

naf_literal : NAF? (classical_literal | builtin_atom | asap_external | asap_event);

asap_action : ASAP_DO DOT MINUS? id (PAREN_OPEN terms PAREN_CLOSE)?;

asap_fluent : fluent=term AT (ASAP_PREV | ASAP_NEXT) EQUAL value=term;

asap_external : ASAP_XT DOT id (SQUARE_OPEN input=terms? SQUARE_CLOSE)? (PAREN_OPEN output=terms? PAREN_CLOSE)?;

asap_event : ASAP_EV DOT id (PAREN_OPEN terms? PAREN_CLOSE)?;

classical_literal : MINUS? id (PAREN_OPEN terms? PAREN_CLOSE)? | asap_fluent;

builtin_atom : term binop term;

binop : EQUAL | UNEQUAL | LESS | GREATER | LESS_OR_EQ | GREATER_OR_EQ;

terms : term (COMMA terms)?;

term : id (PAREN_OPEN terms? PAREN_CLOSE)?
     | NUMBER
     | STRING
     | variable
     | ANONYMOUS_VARIABLE
     | PAREN_OPEN term PAREN_CLOSE
     | MINUS term
     | term arithop term
     | gringo_range;    // syntax extension

gringo_range : (NUMBER | variable | id) DOT DOT (NUMBER | variable | id);    // NOT Core2 syntax, but widespread

gringo_sharp : SHARP ~(DOT)* DOT; // NOT Core2 syntax, but widespread, matching not perfect due to possible earlier dots

basic_terms : basic_term (COMMA basic_terms)? ;

basic_term : ground_term | variable_term;

ground_term : /*SYMBOLIC_CONSTANT*/ id | STRING | MINUS? NUMBER;

variable_term : variable | ANONYMOUS_VARIABLE;

arithop : PLUS | MINUS | TIMES | DIV;

ASAP_EV : 'Ev';
ASAP_XT : 'Xt';
ASAP_PREV : 'prev';
ASAP_NEXT : 'next';

ANONYMOUS_VARIABLE : '_';
DOT : '.';
COMMA : ',';
QUERY_MARK : '?';
COLON : ':';
SEMICOLON : ';';
OR : '|';
NAF : 'not';
CONS : ':-';
WCONS : ':~';
PLUS : '+';
MINUS : '-';
TIMES : '*';
DIV : '/';
AT : '@';
SHARP : '#'; // NOT Core2 syntax but gringo

PAREN_OPEN : '(';
PAREN_CLOSE : ')';
SQUARE_OPEN : '[';
SQUARE_CLOSE : ']';
CURLY_OPEN : '{';
CURLY_CLOSE : '}';
EQUAL : '=';
UNEQUAL : '<>' | '!=';
LESS : '<';
GREATER : '>';
LESS_OR_EQ : '<=';
GREATER_OR_EQ : '>=';

AGGREGATE_COUNT : '#count';
AGGREGATE_MAX : '#max';
AGGREGATE_MIN : '#min';
AGGREGATE_SUM : '#sum';


id : ID | ASAP_PREV | ASAP_NEXT;
variable : VARIABLE | ASAP_DO | ASAP_XT | ASAP_EV;
ASAP_DO : 'Do' ('_' ( 'A'..'Z' | 'a'..'z' | '0'..'9' | '_' )+)?;

ID : ('a'..'z') ( 'A'..'Z' | 'a'..'z' | '0'..'9' | '_' )* ;
VARIABLE : ('A'..'Z') ( 'A'..'Z' | 'a'..'z' | '0'..'9' | '_' )* ;
NUMBER : '0' | ('1'..'9') ('0'..'9')*;
STRING : '"' ( '\\"' | . )*? '"';

COMMENT : '%' ~[\r\n]* -> channel(HIDDEN);
MULTI_LINE_COMMEN : '%*' .*? '*%' -> channel(HIDDEN);
BLANK : [ \t\r\n\f]+ -> channel(HIDDEN) ;
