import org.antlr.v4.runtime.*;

import java.io.IOException;

/**
 * Created by Antonius Weinzierl on 5/20/16.
 */
public class Main {

    static boolean parsingError;

    public static void main(String[] args) throws IOException {

        boolean isHexTarget = true;
        // parse command line arguments
        if( args.length > 0) {
            if( args[0].startsWith("-target=")) {
                if( args[0].endsWith("hex") )
                    isHexTarget= true;
                if( args[0].endsWith("clasp"))
                    isHexTarget = false;
            }
        }

        // prepare parser
        ASAP_ParserLexer lexer = new ASAP_ParserLexer(new ANTLRInputStream(System.in));
        CommonTokenStream toks = new CommonTokenStream(lexer);
        ASAP_ParserParser parser = new ASAP_ParserParser(toks);
        // record eventual parsing errors
        parsingError = false;
        parser.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object o, int i, int i1, String s, RecognitionException e) {
                parsingError = true;
            }
        });

        // run parser
        ASAP_ParserParser.ProgramContext programContext = parser.program();


        if( parsingError ) {
            System.err.println("Error while parsing input ASAP program, see errors above.");
            System.exit(-1);
        }

        Vocabulary vocabulary = lexer.getVocabulary();
        AsapRewriter visitRewriter = new AsapRewriter(vocabulary);
        if( isHexTarget) {
            visitRewriter.setHexTarget();
        } else {
            visitRewriter.setClaspTarget();
        }
        String rewProg = visitRewriter.visit(programContext);

        System.out.println("% ASAP Rewriter.\n" +
                "% Usage: [-target=hex/clasp]");
        System.out.println(rewProg);
    }
}
