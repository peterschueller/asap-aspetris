import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Antonius Weinzierl on 5/23/16.
 */
public class AsapRewriter extends ASAP_ParserBaseVisitor<String> {

    // some default values
    public static final String actionPredicateName = "action";
    public static final String eventPredicateName = "event";
    public static final String fluentPredicateName = "val";
    public static final String actionDefaultOrder = "0";
    public static final String orderExceptionPredicateName = "order_exception";
    private static boolean isHexTarget = true;
    private static boolean isClaspTarget = false;

    private Vocabulary vocabulary;  // the tokens and their names of the lexer

    private Set<String> orders; // orders of ASAP actions encountered in the program

    AsapRewriter(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
    }

    public void setHexTarget() {
        isHexTarget = true;
        isClaspTarget = false;
    }

    public void setClaspTarget() {
        isHexTarget = false;
        isClaspTarget = true;
    }

    @Override
    protected String defaultResult() {
        return "";
    }

    @Override
    public String visitTerminal(TerminalNode node) {
        // return symbol/token text, and add
        // whitespaces (necessary after 'not', nice after and before several other symbols)
        String tokenName = vocabulary.getSymbolicName(node.getSymbol().getType());
        String ret;
        switch (tokenName) {
            // symbols preceeded by WS
            case "NAF":
            case "COMMA":
            case "AGGREGATE_COUNT":
            case "AGGREGATE_MAX":
            case "AGGREGATE_MIN":
            case "AGGREGATE_SUM":
                ret = node.getText()+" ";
                break;
            // symbols surrounded by WS
            case "SEMICOLON":
            case "COLON":
            case "OR":
            case "CONS":
            case "WCONS":
                ret = " "+node.getText()+" ";
                break;
            // everything else is just the original text
            default : ret = node.getText();
        }
        return ret;
    }

    @Override
    protected String aggregateResult(String aggregate, String nextResult) {
        return aggregate+nextResult;
    }

    @Override
    public String visitStatement(ASAP_ParserParser.StatementContext ctx) {
        return super.visitStatement(ctx)+"\n";  // add newline after each rule/statement
    }

    @Override
    public String visitAsap_action(ASAP_ParserParser.Asap_actionContext ctx) {
        // asap_action : ASAP_DO DOT MINUS? id (PAREN_OPEN terms PAREN_CLOSE)?;
        String doplusorder = ctx.ASAP_DO().getText();
        String order;
        if( !doplusorder.contains("_")) {
            order = actionDefaultOrder;
        } else {
            order = doplusorder.substring(doplusorder.indexOf("_")+1); // order is everything after first underscore
            // record (non-integer) order names
            if( !order.matches("\\d+") ) {
                orders.add(order);
            }
        }
        String action = Objects.toString(ctx.MINUS(),"") + visitId(ctx.id()) +
                        Objects.toString(ctx.PAREN_OPEN(), "") + (ctx.terms()!=null ? visitTerms(ctx.terms()) :"") + Objects.toString(ctx.PAREN_CLOSE(), "");
        return actionPredicateName + "(" + action + ", " + order + ")";
    }

    @Override
    public String visitAsap_fluent(ASAP_ParserParser.Asap_fluentContext ctx) {
        // asap_fluent : fluent=term AT (ASAP_PREV | ASAP_NEXT) EQUAL value=term;
        int time = (ctx.ASAP_PREV()!=null ? 1 : 0); // prev becomes 1, next 0
        return fluentPredicateName + "(" +visitTerm(ctx.fluent)+", "+time+", "+visitTerm(ctx.value)+")";
    }

    @Override
    public String visitAsap_external(ASAP_ParserParser.Asap_externalContext ctx) {
        // asap_external : ASAP_XT DOT id (SQUARE_OPEN input=terms? SQUARE_CLOSE)? (PAREN_OPEN output=terms? PAREN_CLOSE)?;
        if( isHexTarget ) {
            return "&"+visitId(ctx.id())+"["+ (ctx.input!=null ? visitTerms(ctx.input):"") + "]" + "(" + (ctx.output!=null ? visitTerms(ctx.output) :"") + ")";
        }
        if( isClaspTarget ) {
            return "@"+visitId(ctx.id())+"("+ (ctx.input!=null ? visitTerms(ctx.input) :"") +" ) = " + "(" + (ctx.output!=null ? visitTerms(ctx.output) :"") + ")";
        }
        throw new RuntimeException("Encountered uncovered target case while rewriting ASAP externals.");
    }

    @Override
    public String visitAsap_event(ASAP_ParserParser.Asap_eventContext ctx) {
        // asap_event : ASAP_EV DOT id (PAREN_OPEN terms? PAREN_CLOSE)?;
        if( isHexTarget ) {
            return "&"+eventPredicateName + "[]("+visitId(ctx.id()) + (ctx.terms()!=null ? "(" + visitTerms(ctx.terms()) + ")" : "" ) + ")";
        }
        if ( isClaspTarget ) {
            return "@"+eventPredicateName + "( ) = ("+visitId(ctx.id()) + (ctx.terms()!=null ? "(" + visitTerms(ctx.terms()) + ")" : "" ) + ")";
        }
        throw new RuntimeException("Encountered uncovered target case while rewriting ASAP events.");
    }

    @Override
    public String visitProgram(ASAP_ParserParser.ProgramContext ctx) {
        orders = new HashSet<>();
        String rew_prog = super.visitProgram(ctx);  // run standard visitor

        // construct rules ensuring transitivity, reflexivity, and antisymmetry of orders encountered in ASAP actions
        String order_rules = "";
        if ( !orders.isEmpty() ) {
            order_rules += "% Rules ensuring transitivity, reflexivity, and antisymmetry of orders encountered in ASAP actions below.\n";
            for (String ordername :
                    orders) {
                // reflexivity
                order_rules += ordername+"(X,X) :- " + ordername+"(X,_).\n";
                order_rules += ordername+"(Y,Y) :- " + ordername+"(_,Y).\n";
                // transitivity
                order_rules += ordername+"(X,Z) :- " + ordername+"(X,Y), "+ordername+"(Y,Z).\n";
                // antisymmetry
                order_rules += orderExceptionPredicateName+"("+ordername+") :- " + ordername+"(X,Y), "+ordername+"(Y,X), X != Y.\n";
            }
        }
        return rew_prog + order_rules;
    }
}
