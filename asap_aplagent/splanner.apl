% This file is part of APLAgent at http://mbal.tk/APLAgentMgr/ which is distributed under GPL

%
% IMPORTED
% ========
%
% Constants:
%
%  currtime: current time step
%  lasttime: last time step
%
%
% Predicates:
%
%  plan/0: the agent is allowed to generate plans.
%  selected_goal(G) : generated plans must achieve goal G.
%
%
%

%
% Simple Planner -- plans only one action per time step
%

time(0..lasttime).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Planner

{ o(A,T) : agent_act(A) }1 :- plan,
			      time(T),
			      T<lasttime,
			      T>=currtime.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Goal satisfaction

goal :- selected_goal(G), achieved(G,lasttime).

:- plan, not goal.
