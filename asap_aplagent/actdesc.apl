% This file is part of APLAgent at http://mbal.tk/APLAgentMgr/ which is distributed under GPL

%
% IMPORTED
% ========
%
% Constants:
%
%  currtime: current time step
%  lasttime: last time step
%
%
% EXPORTED
% ========
%
% Types:
%
%  fluent(F):    F is a fluent
%  agent_act(A): A is an agent action
%  exog_act(A):  A is an exogenous action
%
%

time(0..lasttime).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

element(1..3).

switch(sw(E)) :- element(E).
bulb(b(E)) :- element(E).


fluent(on(B)) :- bulb(B).
fluent(up(SW)) :- switch(SW).

agent_act(flip(SW)) :- switch(SW).

literal(F) :- fluent(F).
literal(neg(F)) :- fluent(F).

opposite(F,neg(F)) :- fluent(F).
opposite(neg(F),F) :- fluent(F).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Action Description

h(up(SW),T2) :- time(T1), time(T2),
		T2 = T1 + 1,
		switch(SW),
		h(neg(up(SW)),T1),
		o(flip(SW),T1).

h(neg(up(SW)),T2) :- time(T1), time(T2),
		T2 = T1 + 1,
		switch(SW),
		h(up(SW),T1),
		o(flip(SW),T1).

h(on(b(E)),T) :- time(T),
		 element(E),
		 h(up(sw(E)),T).

h(neg(on(b(E))),T) :- time(T),
		      element(E),
		      h(neg(up(sw(E))),T).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inertia

h(L,T2) :- time(T1), time(T2),
	   T2 = T1 + 1,
	   opposite(L,NL),
	   h(L,T1),
	   not h(NL,T2).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Consistency axiom

:- time(T), opposite(L,NL),
   h(L,T), h(NL,T).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Connection with Observations

h(L,0) :- literal(L),
	  obs(L,0).

action(A) :- agent_act(A).
action(A) :- exog_act(A).

o(A,T) :- time(T),
	  action(A),
	  hpd(A,T).

:- time(T), opposite(L,NL),
   h(L,T), obs(NL,T).


