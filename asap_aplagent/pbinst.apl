% This file is part of APLAgent at http://mbal.tk/APLAgentMgr/ which is distributed under GPL

%
% IMPORTED
% ========
%
% Constants:
%
%  currtime: current time step
%  lasttime: last time step
%
%
% EXPORTED
% ========
%
%  Types:
%
%   goal(G): G is a goal
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition of goals

%%% Goal allBulbsOn
goal(allBulbsOn).

one_off(T) :- time(T), bulb(B),
	      h(neg(on(B)),T).

all_on(T) :- time(T),
	     not one_off(T).

achieved(allBulbsOn,T) :- time(T),
			  all_on(T).


%%% Goal allBulbsOff
goal(allBulbsOff).

one_on(T) :- time(T), bulb(B),
	     h(on(B),T).

all_off(T) :- time(T),
	      not one_on(T).

achieved(allBulbsOff,T) :- time(T),
			   all_off(T).
